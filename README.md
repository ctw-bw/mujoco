# MuJoCo Installer #

This repositoy contains a CMake wrapper for MuJoCo (<http://www.mujoco.org/>).

MuJoCo itself is not included in this script, it will be download automatically.

## Install ##

To install MuJoCo using this wrapper, run:

```
chmod u+x get_mujoco
./get_mujoco  # This will download the official .zip and refactor some of the files
mkdir build && cd build
cmake ..
make install
```

And that's it. You might need to run `sudo make install` instead, to install it under `/usr/local`.

You can use standard CMake flags, for example to change the target directory:

```
cmake .. -DCMAKE_INSTALL_PREFIX=~/.local
```

Will install to `~/.local/lib/libmujoco.so`, etc.

## Build examples ##

To build the examples, run:

```
mkdir examples/build
cd examples/build
cmake ..
make
```

Inside the directory the new binaries should be available.

If you changed the MuJoCo installation directory, don't forget to use:

```
cmake .. -DCMAKE_PREFIX_PATH=~/.local
```

## Differences with MuJoCo documentation ##

MuJoCo as a library is not changed. But this script will put the public headers in a subdirectory (as is conventional), so your scripts should include it like:

```
#include <mujoco/mujoco.h>
#include <mujoco/glfw3.h>
// ...
```
