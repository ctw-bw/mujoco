#!/bin/bash

wget -N https://www.roboti.us/download/mujoco200_linux.zip

# Headers
unzip -j -o mujoco200_linux.zip "mujoco200_linux/include/*" -d "include/mujoco/"

# Binaries
unzip -j -o mujoco200_linux.zip "mujoco200_linux/bin/*" -d "bin/"

# Libraries
mv bin/{*.so,*.so.*,*.a} lib/
# Also create version without number
cp lib/libglfw{.so.3,.so}

# Doc
unzip -j -o mujoco200_linux.zip "mujoco200_linux/doc/*" -d "doc/"

# Samples
unzip -j -o mujoco200_linux.zip "mujoco200_linux/sample/*.cpp" -d "examples/src/"
mv include/mujoco/uitools.* examples/src

for i in examples/src/*; do
    sed -i 's#"mujoco.h"#<mujoco/mujoco.h>#g' $i
    sed -i 's#"glfw3.h"#<mujoco/glfw3.h>#g' $i
    sed -i 's#"mjxmacro.h"#<mujoco/mjxmacro.h>#g' $i
done
