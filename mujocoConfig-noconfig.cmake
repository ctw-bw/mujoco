#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "mujoco::mujoco200" for configuration ""
set_property(TARGET mujoco::mujoco200 APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(mujoco::mujoco200 PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libmujoco200.so"
  IMPORTED_SONAME_NOCONFIG "libmujoco200.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS mujoco::mujoco200 )
list(APPEND _IMPORT_CHECK_FILES_FOR_mujoco::mujoco200 "${_IMPORT_PREFIX}/lib/libmujoco200.so" )

# Import target "mujoco::mujoco200nogl" for configuration ""
set_property(TARGET mujoco::mujoco200nogl APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(mujoco::mujoco200nogl PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libmujoco200nogl.so"
  IMPORTED_SONAME_NOCONFIG "libmujoco200nogl.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS mujoco::mujoco200nogl )
list(APPEND _IMPORT_CHECK_FILES_FOR_mujoco::mujoco200nogl "${_IMPORT_PREFIX}/lib/libmujoco200nogl.so" )

# Import target "mujoco::glew" for configuration ""
set_property(TARGET mujoco::glew APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(mujoco::glew PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libglew.so"
  IMPORTED_SONAME_NOCONFIG "libglew.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS mujoco::glew )
list(APPEND _IMPORT_CHECK_FILES_FOR_mujoco::glew "${_IMPORT_PREFIX}/lib/libglew.so" )

# Import target "mujoco::glewegl" for configuration ""
set_property(TARGET mujoco::glewegl APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(mujoco::glewegl PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libglewegl.so"
  IMPORTED_SONAME_NOCONFIG "libglewegl.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS mujoco::glewegl )
list(APPEND _IMPORT_CHECK_FILES_FOR_mujoco::glewegl "${_IMPORT_PREFIX}/lib/libglewegl.so" )

# Import target "mujoco::glewosmesa" for configuration ""
set_property(TARGET mujoco::glewosmesa APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(mujoco::glewosmesa PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libglewosmesa.so"
  IMPORTED_SONAME_NOCONFIG "libglewosmesa.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS mujoco::glewosmesa )
list(APPEND _IMPORT_CHECK_FILES_FOR_mujoco::glewosmesa "${_IMPORT_PREFIX}/lib/libglewosmesa.so" )

# Import target "mujoco::glfw" for configuration ""
set_property(TARGET mujoco::glfw APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(mujoco::glfw PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libglfw.so.3"
  IMPORTED_SONAME_NOCONFIG "libglfw.so.3"
  )

list(APPEND _IMPORT_CHECK_TARGETS mujoco::glfw )
list(APPEND _IMPORT_CHECK_FILES_FOR_mujoco::glfw "${_IMPORT_PREFIX}/lib/libglfw.so.3" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
